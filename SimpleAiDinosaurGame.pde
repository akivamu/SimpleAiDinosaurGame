int VIEWPORT_W = 800;
int VIEWPORT_H = 800;
int GROUND_Y = 100;
PVector ENEMY_INIT_POS = new PVector(VIEWPORT_W, 0);
PVector DINO_INIT_POS = new PVector(VIEWPORT_W / 3, 0);
int INPUT_SIZE = 3; // Dist to enemy; enemy W; enemy H; Bias
int OUTPUT_SIZE = 2; // DO_NOTHING; JUMP
int GENERATION_SIZE = 100;

Generation generation = new Generation(0, GENERATION_SIZE, null);
Enemy enemy = new Enemy(ENEMY_INIT_POS);
PFont font;
void setup() {
  size(800, 800);
  frameRate(100);
  font = createFont("Arial", 16, true);
}



float lastDistToEnemy = 9999;

boolean isCollide(Dino dino, Enemy enemy) {

  float playerLeft = dino.pos.x;
  float playerRight = dino.pos.x + dino.w;
  float thisLeft = enemy.pos.x;
  float thisRight = enemy.pos.x +enemy.w;

  if ((playerLeft<= thisRight && playerRight >= thisLeft ) || (thisLeft <= playerRight && thisRight >= playerLeft)) {
    float playerDown = dino.pos.y;
    float thisUp = enemy.pos.y + enemy.h;
    if (playerDown <= thisUp) {
      return true;
    }
  }
  return false;
}


void draw() {
  background(255);

  generation.update(enemy);

  if (generation.isAllDone()) {

    Dino best = generation.findBestSubject();
    generation = new Generation(generation.id + 1, GENERATION_SIZE, best);
    enemy = new Enemy(ENEMY_INIT_POS);
  }

  enemy.update();
  if (enemy.pos.x < 0) {
    enemy = new Enemy(ENEMY_INIT_POS);
  }

  generation.renderAllSubjects();
  enemy.render();

  // Draw info
  textFont(font, 16);
  fill(0);        
  text("Gen #"+ generation.id + ", score: "+ generation.score + ", alive: "+generation.aliveCount, 10, 50);
}
