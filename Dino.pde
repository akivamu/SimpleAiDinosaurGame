
class Enemy {
  int w;
  int h;
  float veloX = -3;
  PVector pos;

  Enemy(PVector initPos) {
    pos = initPos.copy();
    w = (int)random(20, 100);
    h = (int)random(100, 130);
  }

  void update() {
    pos.add(veloX, 0);
  }

  void render() {
    fill(#FF0000);
    rect(pos.x, VIEWPORT_H - GROUND_Y - h - pos.y, w, h);
  }
}

class Dino {
  Brain brain;
  int w = 20;
  int h = 20;
  PVector pos;
  float veloY = 0;
  float pulseY = 10;
  float gravity = 0.3;
  int score = 0;
  float rating = 0;
  boolean isActive = true;
  float directionToEnemy = -1;


  Dino() {
    pos = DINO_INIT_POS.copy();

    // New brain
    int[] nodesInLayer = new int[2];
    nodesInLayer[0] = INPUT_SIZE;
    nodesInLayer[1] = OUTPUT_SIZE;

    Node[][] network = new Node[2][max(INPUT_SIZE, OUTPUT_SIZE)];

    for (int i=0; i< INPUT_SIZE; i++) {
      Node node = new Node();
      node.layerIndex = 0;
      network[0][i] = node;
    }

    for (int i=0; i< OUTPUT_SIZE; i++) {
      Node node = new Node();
      node.layerIndex = 1;

      // Add prev nodes to node
      int inputNodes = INPUT_SIZE;
      node.inputNodeIndexs = new int[inputNodes];
      node.weights = new float[inputNodes];
      for (int j=0; j< inputNodes; j++) {
        node.inputNodeIndexs[j] = j;
        node.weights[j] = random(-1, 1);
      }

      network[1][i] = node;
    }

    //println("network size="+(nodesInLayer[0] + nodesInLayer[1]));

    brain = new Brain(nodesInLayer, network);
  }

  Dino giveBirth() {
    Dino newDino = new Dino();
    newDino.brain = brain.clone();
    return newDino;
  }

  int[] collectInput(Enemy enemy) {
    int[] inputs = new int[INPUT_SIZE];
    inputs[0] = (int)dist(pos.x, pos.y, enemy.pos.x, enemy.pos.y);
    inputs[1] = enemy.w;
    inputs[2] = enemy.h;
    return inputs;
  }

  void update(Enemy nextEnemy) {
    // Only make decision if in ground
    if (pos.y == 0) {
      int[] inputs = collectInput(nextEnemy);
      float[] outputs = brain.makeDecision(inputs);

      // Find largest action
      int maxIndex = 0;
      float max = outputs[maxIndex];
      for (int i=0; i<outputs.length; i++) {
        if (max < outputs[i]) {
          max = outputs[i];
          maxIndex = i;
        }
      }

      switch (maxIndex) {
      case 0:
        break;
      case 1:
        jump();
        break;
      }
    }

    pos.y += veloY;
    // Gravity
    if (pos.y > 0) {
      veloY -= gravity;
    } else {
      veloY = 0;
      pos.y = 0;
    }

    // Check score
    float direction = pos.x - (nextEnemy.pos.x + nextEnemy.w) ;
    if (direction * directionToEnemy < 0) {
      // Just pass
      score++;
      //println("Score: "+score);
    }
    directionToEnemy = direction;
  }

  void jump() {
    veloY = pulseY;
  }

  void render() {
    fill(#0000FF);
    rect(pos.x, VIEWPORT_H - GROUND_Y - h - pos.y, w, h);
  }
}

class Brain {
  int numberOfLayers;
  int[] nodesInLayer;
  Node[][] network;

  Brain(int[] nodesInLayer, Node[][] network) {
    this.numberOfLayers = nodesInLayer.length;
    this.nodesInLayer = nodesInLayer;
    this.network = network;
  }

  float[] makeDecision(int[] inputs) {

    // 1st layer is input
    Node[] firstLayerNodes = network[0];
    for (int i=0; i<inputs.length; i++) {
      firstLayerNodes[i].output = inputs[i];
    }

    // Forward propagate
    for (int layer=1; layer<numberOfLayers; layer++) {
      Node[] nodes = network[layer];
      for (int nodeIndex=0; nodeIndex< nodesInLayer[layer]; nodeIndex++) {
        nodes[nodeIndex].calculateOutput(network[layer - 1]);
      }
    }

    // Extract outputs of last layer
    Node[] lastLayerNodes = network[numberOfLayers - 1];
    float[] outputs = new float[OUTPUT_SIZE];

    for (int i=0; i<OUTPUT_SIZE; i++) {
      outputs[i] =lastLayerNodes[i].output;
    }

    return outputs;
  }

  void mutate() {
    for (int i=0; i<network.length; i++) {
      for (int j=0; j<network[0].length; j++) {
        Node node = network[i][j];
        if (node != null) {
          node.mutate();
        }
      }
    }
  }

  Brain clone() {
    Node[][] network = new Node[numberOfLayers][this.network[0].length];
    for (int i=0; i<network.length; i++) {
      for (int j=0; j<network[0].length; j++) {
        if (this.network[i][j] != null) {
          network[i][j] = this.network[i][j].clone();
        }
      }
    }

    Brain brain = new Brain(this.nodesInLayer.clone(), network);


    return brain;
  }
}


class Node {
  int layerIndex;
  int[] inputNodeIndexs;
  float[] weights;
  float output;

  void calculateOutput(Node[] prevLayerNodes) {
    this.output = 0;
    for (int i=0; i<inputNodeIndexs.length; i++) {
      int prevNodeIndex = inputNodeIndexs[i];
      this.output += weights[i] * prevLayerNodes[prevNodeIndex].output;
    }
  }

  Node clone() {
    Node node = new Node();
    node.layerIndex = layerIndex;
    node.inputNodeIndexs = inputNodeIndexs != null ? inputNodeIndexs.clone() : null;
    node.weights = weights != null ? weights.clone() : null;

    return node;
  }

  void mutate() {
    if (weights != null) {
      for (int k=0; k<weights.length; k++) {
        if (random(1) < 0.1) { // 10% replace
          weights[k] = random(-1, 1);
        } else {
          weights[k] += randomGaussian()/50;
          weights[k] = min(weights[k], 1);
          weights[k] = max(weights[k], -1);
        }
      }
    }
  }
}
