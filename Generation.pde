
class Generation {
  int id;
  private Dino[] subjects;
  private Dino refSubject;
  int score = 0;
  int aliveCount = 0;

  Generation(int id, int size, Dino refSubject) {
    this.id = id;
    subjects = new Dino[size];

    for (int i = 0; i< size; i++) {
      subjects[i] = new Dino();
    }
    
    this.refSubject = refSubject;
    if (refSubject != null) {
      println("new gen from best score: "+refSubject.score);
      subjects[0] = refSubject.giveBirth();
      for (int i = 1; i< size; i++) {
        subjects[i] = refSubject.giveBirth();
        subjects[i].brain.mutate();
      }
    }
  }

  void update(Enemy enemy) {
    // Check all subjects
    aliveCount = 0;
    for (Dino subject : subjects) {
      updateSubject(subject, enemy);
      if (subject.score > score) {
        score = subject.score;
      }
      if (subject.isActive) aliveCount++;
    }
  }

  private void updateSubject(Dino subject, Enemy enemy) {
    // Check state
    if (subject.isActive) {

      // Collide
      if (isCollide(subject, enemy)) {
        subject.isActive = false;
        subject.pos.y = 0;
      }
    }

    // Make decision and move if active
    if (subject.isActive) {
      subject.update(enemy);
    }
  }

  void renderAllSubjects() {
    for (Dino subject : subjects) {
      subject.render();
    }
  }

  Dino findBestSubject() {
    float bestRating = 0;
    Dino bestSubject = subjects[0];
    for (Dino subject : subjects) {
      float rating = calculateSubjectRating(subject);
      if (rating > bestRating) {
        bestSubject = subject;
        bestRating = rating;
      }
    }
    
    if (refSubject != null && bestRating < refSubject.rating) {
      bestSubject = refSubject;
    }
    
    return bestSubject;
  }

  boolean isAllDone() {
    for (Dino subject : subjects) {
      if (subject.isActive) {
        return false;
      }
    }
    return true;
  }

  private float calculateSubjectRating(Dino subject) {
    subject.rating = subject.score * subject.score;
    return subject.rating;
  }
}
